var exp = require('express')();
var http = require('http').Server(exp)
var io = require('socket.io')(http)

// Serve index.html
exp.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

// Set the listening port to 4321
http.listen(4321, function(){
    console.log('Server is listening on *:4321')
});

// Socket.IO implementation
io.on('connection', function(socket) {
    console.log('New user!')

    socket.on('set_username', function(msg) {
        console.log('Username set:', msg.username)
        socket.Name = msg.username
        // Let everyone else know that a new user has entered the chat
        socket.broadcast.emit('message', { name: null, txt: socket.Name + ' has entered the chat!' })
    });

    socket.on('message', function(msg) {
        console.log('New message:', socket.Name, msg.msg)
        // Emit the message to everyone except the user who sent it
        socket.broadcast.emit('message', { name: socket.Name, txt: msg.msg })
    });

    socket.on('disconnect', function() {
        console.log('User disconnected...', socket.Name)
        // Let everyone else know that a new user has left the chat
        io.emit('message', { name: null, txt: socket.Name + ' has left the chat!' })
        socket.disconnect()
    });
});