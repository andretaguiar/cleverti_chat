#WEBSOCKET (PART 1) - LEARN THE ROPES

I bet that every programmer that ever played an online multiplayer game or used a chat for the first time wondered how was it possible that the communication was so quick!
With today's technology it has become easier than ever. Let's have a look at a possible way to achieve this goal. 

#Introduction

This article will be split into 2 parts:

Part 1 – **Learn the ropes**

Here we will discuss the usage of websocket in a palpable context, while we create a working example of a simple chat application using **Socket.IO** - an implementation of websocket for **Node.js**.

Part 2 – **Let's play together**

Using the knowledge acquired on part 1, we will create a multiplayer server in **Node.js** using **Socket.IO**. Afterwards, we will create a simple rock-paper-scissors game. Also, we will look into the concepts of **match-making** and **game rooms**.

#Learn the ropes

Websocket is a communication protocol that allows TCP/IP communication between a web client (e.g., web browser) and a web server, providing a persistent connection and bi-directional messaging between both parties without the overhead of an HTTP request.

The diagram below is a good way to wrap your head around the concept.

![](https://i.imgur.com/ZBpQCyS.png)

##1st phase##

The server will be listening for an initial HTTP call so a **handshake** can take place. Some required validations may be performed accordingly to the server's needs (e.g., region locking, user registration, credential validation, …).

If the validation is successful, the server sends a response to the client telling it that the connection has been established, the client is now **connected** and further communication can be take place without making use of the HTTP protocol.

##2nd phase##

There is now a **persistent connection** between the server and the client.

**Bi-directional** communication is available and either part can send **messages** to one another by sending payloads.

If a client needs to talk to another client that is connected to the same server, the communication is always relayed through the server.

##3rd phase##

If either one of the parts sends a **disconnect** signal or the transmission is **terminated** for any reason (e.g., power surge) the communication channel between machines is lost.

#Chat server

##Description##

For this example, we will use a **[Node.js](https://nodejs.org)** server using the web framework **[express](https://expressjs.com/)** and an implementation of websocket called **[Socket.IO](https://socket.io)**.

This combination may not appropriate for every real-life application but for this example will do just fine.

##Prerequisites##

Install **[Node.js](https://nodejs.org)** if you don't have it already. This will also install **npm**.

We will need to install **express** through the terminal:

`npm install --save express`

Let's also install **Socket.IO** through the terminal:

`npm install --save socket.io`

In this example, we will implement all the server side functionality needed for the chat server.

- Serve index.html we will later create.
- Listen for connection requests on port 4321.
- Listen for miscellaneous method calls (e.g., `set_username`, `message`).
- Listen for disconnection requests.

##Implementation##

Create `chat_server.js` with the following [code](https://codepen.io/andretaguiar/pen/arzBGm).

Let's go to the terminal and start the server:

`node chat_server`

We should get the following output:

`Server is listening on *:4321`

That's it for the server. Now let's create a simple chat page to communicate with our server.

#Chat client

##Description##

As we did in the server we will need to use some external resources, we will use **[Vue.js](https://vuejs.org/)** to help us build our interface, **[Materialize](https://materializecss.com/)** to beautify our page and of course we still need to use **Socket.IO** to connect to our server.

In this example we will implement all the client side functionality needed to use the chat server we created previously.

- Connect/Reconnect to localhost:4321.
- Set the generated username ('User-' + Random number from 0 to 999).
- Send messages.
- Disconnect from the server.

##Prerequesites##

All of the resources will be supplied by CDNs.

##Implementation##

Create `index.html` with the following [code](https://codepen.io/andretaguiar/pen/qGERvQ) or use the codepen Result to test the chat.

##Demo##

###Chat###

![](https://i.imgur.com/fvwurhf.gif)

###Disconnect###

![](https://i.imgur.com/HUL84mt.gif)

#Thank you!#

See you in Part 2 – **Let's play together**!

#Authors#

André Aguiar

#License#

© 2019 Cleverti
